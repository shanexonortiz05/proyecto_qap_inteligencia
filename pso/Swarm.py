import numpy as np
import math
from pso.particle import Particle

''' Clase que contiene la logica de mas alto nivel de PSO  '''


class Swarm:

    def __init__(self, information_instance):
        # Guarda las soluciones tentativas unicas
        self.dictionary_particles = dict()
        # Guarda los datos relacionados con la instancia del problema
        self.information_instance = information_instance
        # Guarda la mejor solucion global encontrada
        self.best_global_solution = np.array([math.inf] * self.information_instance.instance_size)
        # Guarda el fitness de la mejor solucion encontrada
        self.best_fitness = math.inf
        # Guarda las particulas
        self.particle_list = list()
        # Guarda un arreglo base de posiciones a partir de las cuales se permuta
        self.base_array = list(range(self.information_instance.instance_size))

        # Definicion de constantes
        self.number_particles = 20
        # Maximo numero de iteraciones
        self.maximas_iterations = 100

    def run_pso(self):
        ''''
            Este metodo se encarga de encontrar la mejor solucion en base a la logica principal
            del PSO.

                Instanciar Particulas
               Mientras no se cumpla el maximo de iteraciones:
                    Para cada particula:
                        Calcular Fitness de Particula y Actualizar Mejor Personal
                    Calcular mejor global y Actualizar Mejor Fitness
                    Para Cada Particula:
                        Actualizar Velocidad
                        Actualizar Posicion
        '''
        # Poblamos el arreglo de particulas con una cantidad X de particulas
        for i in range(self.number_particles):
            self.particle_list.append(Particle(self.dictionary_particles, self.base_array))

        for i in range(self.maximas_iterations):
            for particula in self.particle_list:
                # Calcula fitness de cada particula y actualiza mejor personal
                particula.update_best_staff(self.information_instance.calculate_cost(particula.tentative_solution))
            # actualizar mejor global
            for particula in self.particle_list:
                if particula.particle_fitness < self.best_fitness:
                    self.best_fitness = particula.particle_fitness
                    self.best_global_solution = particula.tentative_solution

            # Actualizar Velocidad y Posicion
            for particula in self.particle_list:
                particula.update_speed(self.best_global_solution)
                particula.update_position()

            # Cada 10 iteraciones se muestra el mejor fitness
            if i % 10 == 0:
                print('El mejor fitness actual es : ' + str(self.best_fitness))
        # Se imprime la mejor solucion
        print(self.show_solution())

    def show_solution(self):
        chain = '[ '
        for puesto in self.best_global_solution:
            chain = chain + str(puesto) + ' , '
        chain = chain + ' ]'
        return chain
