import random
import numpy as np
import math

'''Clase que contiene la información, y comportamiento de una
    particula del enjambre.'''


class Particle:

    def __init__(self, dictionary_particles, original_arrangement):
        # Arreglo que representa la solucion actual de la particula
        self.tentative_solution = np.copy(original_arrangement)
        # Arreglo que representa la speed actual de la particula
        self.speed = np.zeros(len(original_arrangement))
        # Arreglo que representa la mejor posicion alcanzada por la particula
        self.best_personal_position = np.zeros(len(original_arrangement))
        # Fitness de la particula
        self.particle_fitness = math.inf

        # Coeficientes de impacto
        self.c1 = 1
        self.c2 = 1
        self.speed_maxima = 100

        random.shuffle(self.tentative_solution)
        while dictionary_particles.get(self.generate_identifier(), None) is not None:
            random.shuffle(self.tentative_solution)
        dictionary_particles[self.generate_identifier()] = self.tentative_solution


# El siguiente metodo recolecta datos del diccionario y busca la solución más tentativa
    def generate_identifier(self):
        id = ''
        for i in range(0, len(self.tentative_solution)):
            id = id + str(self.tentative_solution[i])
        return id

# Funcion que actualiza la velocidad de la mejor solución

    def update_speed(self, mejor_posicion_global):
        for i in range(0, len(self.speed)):
            speed_parcial = self.speed[i] + random.random() * self.c1 * (
                        self.best_personal_position[i] - self.tentative_solution[i]) + random.random() * self.c2 * (
                                        mejor_posicion_global[i] - self.tentative_solution[i])
            if abs(speed_parcial) <= self.speed_maxima:
                self.speed[i] = speed_parcial
        # print(self.speed)
# Funcion que actualiza la posición con la solución más tentativa y la formula de velocidad
    def update_position(self):
        for i in range(0, len(self.tentative_solution)):
            if random.random() < float(self.speed[i]) / 100.0:
                if int(self.speed[i]) > 0 and i < len(self.tentative_solution) - 1:
                    aux = self.tentative_solution[i]
                    self.tentative_solution[i] = self.tentative_solution[i + 1]
                    self.tentative_solution[i + 1] = aux
                    speed_aux = self.speed[i]
                    self.speed[i] = self.speed[i + 1]
                    self.speed[i + 1] = speed_aux
                elif int(self.speed[i]) < 0 and i > 0:
                    aux = self.tentative_solution[i]
                    self.tentative_solution[i] = self.tentative_solution[i - 1]
                    self.tentative_solution[i - 1] = aux
                    speed_aux = self.speed[i]
                    self.speed[i] = self.speed[i - 1]
                    self.speed[i - 1] = speed_aux


#Funcion que actualiza el mejor fitness en base a la mejor posición personal y solución tentativa
    def update_best_staff(self, fitness):
        if fitness < self.particle_fitness:
            self.particle_fitness = fitness
            self.best_personal_position = self.tentative_solution
